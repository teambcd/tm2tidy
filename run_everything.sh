# this script will...
#    1. preformat the LUE model output to the TM grid
#    2. run the TM2 model
#

#!/bin/bash

#Paths & parameters
Project_path=~/soil_moist/tm2tidy

Prep_path=TRANSPORT_prep/
TM_path=TRANSPORT/
FortCompiler=gfortran

# Funtions
setDir () {
	cd $Project_path
	cd $1
}

sourceFortA () {
	$FortCompiler *.f
	./a.*
	rm ./a.*
}

PerformStep () {
	setDir $1
	sourceFortA
	echo $2
}

# perform step 1:
PerformStep $Prep_path "!!!!!!!!!!! finished the formatting (nepLUE_orig.f) !!!!!!!!!!!!"

# perform step 2
PerformStep $TM_path "!!!!!!!!!!! finished running TM2 (TM2.f) !!!!!!!!!!!!"
