c
c  convert LUE model fluxes onto Tm2 grid
c                                  
      implicit none
 
      real lon,lat,nep(36,24,12),mnpp(12),mnee(12),mrh(12),c_flux
      integer ilon,ilat,m 
      real n(36,24),area
      real days(12)
      
      data (days(m),m=1,12)
     * / 31.,28.,31.,30.,31.,30.,31.,31.,30.,31.,30.,31. / 


*      open(1,
*     * file=
*     * '../modeledFlux/npp_b3.out',
*     * status='old',err=901)
*      open(2,
*     * file=
*     * '../modeledFlux/rh_b3.out',
*     * status='old',err=902)
      open(3,
     * file='./modeledFlux/nee_for_TM2.csv',
     * status='old',err=900)

      open(4,file='../TRANSPORT/nep_orig.out',status='unknown')

      do ilon=1,36
        do ilat=1,24
          do m=1,12
             nep(ilon,ilat,m)=0.0
          enddo
          n(ilon,ilat)=0.0
        enddo
      enddo 

100   continue
*      read(1,*)
*      read(1,*,err=910,end=200) lon,lat,(mnpp(m),m=1,12)
*      read(2,*)
*      read(2,*,err=910,end=200) lon,lat,(mrh(m),m=1,12)
*      Print *, 'mrh is', mrh

      read(3,*,err=910,end=200) lon,lat,(mnee(m),m=1,12)
*      print*,'lon,lat',lon,lat,(mnee(m),m=1,12)
*      print*,'lon,lat',lon,lat,(mrh(m),m=1,12)

c
c     TM2 grid starts (?), -180W, -90S
c

*      ilon=int( (190.0+lon/10.)/10.0 )
*      ilat=int( (lat/10.+97.5)/7.5 )
       ilon=int( (190.0+lon)/10.0 )
       ilat=int( (lat+97.5)/7.5 )
*      print*,'lon,lat',ilon,ilat

*      call pixelarea((lat/10.+0.25),area)
      call pixelarea((lat+0.25),area)
*       print*,'area',area

      do m=1,12
c
c nep in units: kg CO2/pixel molar mass CO2=12+16+16=44
c                                       C  =12
c  multiply fields by 44./12.
*         nep(ilon,ilat,m)=nep(ilon,ilat,m)+(mnpp(m)-mrh(m)-
*     *      (c_flux*1000.0/12.0))*area/1000.0*44.0/12.0  
*        nep(ilon,ilat,m)=nep(ilon,ilat,m)+(mrh(m)-mnpp(m)
*     *      )*(area/1000.0)*(365./days(m))
        nep(ilon,ilat,m)=nep(ilon,ilat,m)+(mnee(m)
     *      )*(area/1000.0)*(365./days(m))
*         n(ilon,ilat)=n(ilon,ilat)+1.0
      enddo     

      goto 100
200   continue     
      

      do m=1,12
         do ilat=1,24 
           write(4,10,err=920) (nep(ilon,ilat,m),ilon=1,36)
10         format(36f20.2)
         enddo
      enddo

      stop 'successful termination'
900   stop 'err: opening NEE file'
901   stop 'err: opening 1'
902   stop 'err: opening 2'
910   stop 'err: reading'
920   stop 'err: writing'
      end    


c!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
c      alberte's method
c                      
*      subroutine pixelarea(lat,area)
*      implicit none
*
*      real area,lat  
*      real pie,dip
*      
*      pie = 4. * ATAN(1.)
*      dip = pie/180. 
*c     0.5 degrees at the equator is 111km    
*      area=((111.0*10**3*0.5)**2)*cos(lat*dip)
*c
*c     TM2 10degrees longitude, 7.5 degrees latitude
**      area=(111.0*10**3*10.0)*(111.0*10**3*7.5)*cos(lat*dip)
*      
*
*      return
*      end    

*************************************************************************** 
c         subroutine calculates the area of the pixel(0.5x0.5)degress
c
      subroutine pixelarea(lat,area)
      implicit none
      real lat,ilat
      double precision pie,re,dip,equ,
     >     sum,earth
       real area

       re = 6.378E6
       pie = 4. * ATAN(1.)
       dip = pie/180.
*       equ = (re**2) * ((2.*pie)/(360./3.75))
       equ = (re**2) * ((2.*pie)/(360./0.5))   

c
c      cross sectional area of a sphere = 4*pie*re**2
c      cross sectional area of one hemisphere = 2*pie*re**2
c      area one 0.5 latitude band = 2*pie*re**2 * (cos(lat)-cos(lat+0.5)) 
c      divide latitude band into 720 (0.5degree longitude)
c  area of 0.5 degree pixel= (1/720)* 2*pie*re**2 * (cos(lat)-cos(lat+0.5)) 
c
       earth = 4. * pie * (re**2) 

 
*       sum = equ * ( COS( (lat * dip) + pie/2.) -
*     >    COS((lat*dip) + (pie/2.) + (5.*pie/360.) ) )   ! 2.5 degree lat
              sum = equ * ( COS( (lat * dip) + pie/2.) -
     >    COS((lat*dip) + (pie/2.) + (pie/360.) ) )  ! 0.5 degree lat
       area=REAL(sum)
              
       return
       end  
          
