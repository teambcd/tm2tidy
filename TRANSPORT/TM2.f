c
c  read ascii format nep(ilon,ilat,m) ilon=1,36; ilat=1,24; m=1,12;
c
C     *
C     *     T M 2
C     *
C     * This program simulates the atmospheric transport by matrix
C     * multiplication. Matrix from Thomas Kaminski. Input files
C     * consist of a set of 12 hdf files with the dimensions 36*24
C     * (names: NET_01.hdf � NET_12.hdf)
C     * and output is a text file with the simulated concentrations.
C     *
C     * (Axel Kleidon, 04.11.1996)
C     *

      PROGRAM TM2
      
      IMPLICIT       none

C     ================================================================+=====

C     * dimension of fluxes

      INTEGER        nmonf,im,jm,ng,nf
      PARAMETER      (nmonf=12,im=36,jm=24,ng=im*jm)
      PARAMETER      (nf=im*jm*nmonf)

C     * dimension of concentrations

      INTEGER        nmonc,nstamat,nc
      PARAMETER      (nstamat=27,nmonc=12,nc=nstamat*nmonc)

C     * transport matrix

      REAL*4         pTM2_1     (nf, 18 * nmonc)
      REAL*4         pTM2_2     (nf,  9 * nmonc)
      REAL*4         pTM2_i     
C     * loop variables

      INTEGER        i, j, k

C     * station labels

      CHARACTER*3    tStatLabel (nstamat)

C     * fluxes

      REAL           pFlux      (nf)
 
C     * concentrations

      REAL           pConc      (nc)
      
C     * hdf variables

      REAL           theData    (im, jm)
      REAL           nep(im,jm,12)
      
      INTEGER        iMonth, iIndex
      INTEGER        ix, iy      
      INTEGER        iRank, iShape(2), iCode
      
*      INTEGER        DFSDGETDATA
      
C     * month texts

      CHARACTER*2   mText(12)
      DATA          mText/'01','02','03','04','05','06',
     .                    '07','08','09','10','11','12'/

C     * dummy variables

      INTEGER        idum
      CHARACTER      tdum
      CHARACTER*80   tdum2
      
      character*80 outnam,filnam,innam

c     PARAMETERS
      integer key,scr
      parameter(key=5,scr=6)
      integer lstr     

C     ================================================================+=====
C     
C       1. Read Transport Matrix - Cray
C     
C     ================================================================+=====

      WRITE (*,*) 'TM2: Reading transport matrices...'

      WRITE (*,*) 'TM2: Reading transport matrices (1)...'      
      OPEN(10, 
     * FILE='mat_mre_18.gad', 
     *         FORM='UNFORMATTED',
     .         ACCESS='DIRECT', RECL=4 * 18 * nmonc * nf)
      
      READ(10, REC=1) pTM2_1
      
      CLOSE(10)

      WRITE (*,*) 'TM2: Reading transport matrices (2)...'
      OPEN(11, 
     * FILE='mat_mre_-9_new.gad',
     *         FORM='UNFORMATTED',
     .         ACCESS='DIRECT', RECL=4 *  9 * nmonc * nf)
      
      READ(11, REC=1) pTM2_2
*      print*,pTM2_2


      CLOSE(11)
      
C     ================================================================+=====
C     
C       2. Read Station Names - Cray
C     
C     ================================================================+=====

      WRITE (*,*) 'TM2: Reading station names...'

      OPEN(12, FILE='staloc_all.d')
      
      DO i = 1, nstamat
      
         READ(12,'(A1,A3)') tdum, tStatLabel(i)
C         READ(12,*) tdum, tStatLabel(i), tdum2
         WRITE(*,*) tStatLabel(i), ' read...'
      
      END DO

      CLOSE(12)

C     ================================================================+=====
C     
C       3. Read HDF flux matrices for each month
C     
C     ================================================================+=====

      write(scr,*) 
      !write(scr,*) 'input filename (assumes .out exten) '
      filnam = 'nep_orig'
      WRITE (*,*) 'TM2: Reading fluxes...'
        
      innam=filnam(1:lstr(filnam))//'.out'
      open(70,file=innam,status='old',err=900)

      do iMonth=1,12 

        do j=1,jm
          read(70,100,err=910) (nep(i,j,iMonth),i=1,36)
100       format(36f20.2)
        enddo    
        print*,iMonth


C     * monthly loop

*      DO iMonth = 1, 12
*
*        iRank          = 2
*        iShape(1)      = im
*        iShape(2)      = jm
*
*        iCode   = DFSDGETDATA('NET_'//mText(iMonth)//'.hdf',
*     .                        iRank, iShape, theData)
*      
*        WRITE (*,*) 'Read Month = ', iMonth, ' --- Error = ', iCode
*        
C       * transfer data 

        DO i = 1, im
        DO j = 1, jm  

         
          iIndex       = (iMonth - 1) * ng + (j - 1) * im + i
*          pFlux(iIndex)= theData(i, j)
          pFlux(iIndex)= nep(i,j,iMonth)
*        print*,pFlux(iIndex)


        END DO
        END DO
        
      END DO

C     ================================================================+=====
C     
C       4. Calculate Transport
C     
C     ================================================================+=====

        WRITE (*,*) 'TM2: Calculating transport...'

        DO j = 1, nc
            pConc(j)       = 0.
            DO i = 1, nf
                IF (j .LE. 18 * nmonc) THEN
                    pTM2_i=pTM2_1(i,j)
                ELSE
                    pTM2_i=pTM2_2(i, j - 18 * nmonc)
                END IF
            
                IF (ISNAN(pTM2_i)) pTM2_i=0.0
                pConc(j)   = pConc(j)+ pTM2_i * pFlux(i)
            END DO
        END DO


C     ================================================================+=====
C     
C       5. Output
C     
C     ================================================================+=====
      WRITE (*,*) 'TM2: Writing output...'   

      outnam=filnam(1:lstr(filnam))//'.tm2-output.out'
      open(20,file=outnam,status='replace',form='formatted')

      WRITE(20,'(A4,27A8)') 'Mon', tStatLabel
      
      DO iMonth = 1, 12
      
        WRITE(20,'(I4,27F8.2)') iMonth,
     *  (pConc((j-1) * nmonf + iMonth), j = 1, nstamat)
      
      END DO

      DO iMonth = 1, 6
      
        WRITE(20,'(I4,27F8.2)') iMonth+12, 
     * (pConc((j-1) * nmonf + iMonth), j = 1, nstamat)
      
      END DO
      
      CLOSE(20)
      
C     ================================================================+=====

      WRITE (*,*) 'TM2: DONE'
      
      STOP    
900   stop 'error: opening'
910   stop 'error: reading'
      END
          

c----------------------------------------------------------------------

      integer function lstr(str)

      implicit none

      character*(*) str
      integer i,l

      l=len(str)

      do 100 i=l,1,-1
         if(str(i:i).eq.' '.or.str(i:i).eq.char(0)) goto 100
         lstr=i
         return
100   continue

      lstr=0

      end   
